class Book
  attr_reader :title
  def title=(str)
   conj = ["in", "the", "of", "and", "a", "an"]
   word_arr = str.split
   @title = word_arr.map.with_index do |word, idx|
    (conj.include?(word) && idx != 0) ? word : word.capitalize
   end.join(' ')
  end
end
