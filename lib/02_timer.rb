require "time"

class Timer

  attr_accessor :seconds

  def initialize(start = 0)
    @seconds = start
  end

  def time_string
   Time.at(seconds).utc.strftime("%H:%M:%S")
  end

end
